\begin{example}
Let $A=(\{q_0,q_1,q_2,q_3,q_4\}, \{a,p\}, \delta, q_0, \{q_4\})$ be a DFA with:\\

\begin{table}[h!]
\begin{tabular}{c|c|c}
  $\delta(q,\sigma)$ & $a$ & $p$\\
  \hline
  $q_0$& $q_2$ & $q_1$\\
  \hline
  $q_1$& $q_4$ & $q_1$\\
  \hline
  $q_2$& $q_2$ & $q_2$\\
  \hline
  $q_3$& $q_4$ & $q_2$\\
  \hline
  $q_4$& $q_2$ & $q_3$
  \hline
\end{tabular}
\end{table}

\begin{center}
\begin{tikzpicture}[>=latex',line join=bevel,]
  \pgfsetlinewidth{1bp}

\begin{scope}
  \pgfsetstrokecolor{black}
  \definecolor{strokecol}{rgb}{1.0,1.0,1.0};
  \pgfsetstrokecolor{strokecol}
  \definecolor{fillcol}{rgb}{1.0,1.0,1.0};
  \pgfsetfillcolor{fillcol}
  \filldraw (0.0bp,0.0bp) -- (0.0bp,211.59bp) -- (314.95bp,211.59bp) -- (314.95bp,0.0bp) -- cycle;
\end{scope}
  \pgfsetcolor{black}
  % Edge: a -> q0
  \draw [->] (4.0266bp,59.148bp) .. controls (12.05bp,59.148bp) and (20.073bp,59.148bp)  .. (38.26bp,59.148bp);
  % Edge: q0 -> q2
  \draw [->] (75.792bp,70.31bp) .. controls (103.1bp,86.644bp) and (156.95bp,118.85bp)  .. (197.83bp,143.3bp);
  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
  \pgfsetstrokecolor{strokecol}
  \draw (154.3bp,108.8bp) node {\textit{a}};
  % Edge: q2 -> q2
  \draw [->] (233.64bp,146.54bp) .. controls (243.74bp,145.19bp) and (252.95bp,147.83bp)  .. (252.95bp,154.44bp) .. controls (252.95bp,158.68bp) and (249.17bp,161.29bp)  .. (233.64bp,162.35bp);
  \draw (256.45bp,154.44bp) node {\textit{a}};
  % Edge: q2 -> q2
  \draw [->] (214.8bp,174.44bp) .. controls (223.8bp,181.93bp) and (223.8bp,196.59bp)  .. (214.8bp,196.59bp) .. controls (208.61bp,196.59bp) and (206.68bp,189.66bp)  .. (214.8bp,174.44bp);
  \draw (214.8bp,204.09bp) node {\textit{p}};
  % Edge: q1 -> q1
  \draw [->] (134.8bp,39.148bp) .. controls (143.8bp,31.0bp) and (143.8bp,15.0bp)  .. (134.8bp,15.0bp) .. controls (128.4bp,15.0bp) and (126.55bp,23.087bp)  .. (134.8bp,39.148bp);
  \draw (134.8bp,7.5bp) node {\textit{p}};
  % Edge: q1 -> q4
  \draw [->] (154.99bp,59.148bp) .. controls (162.69bp,59.148bp) and (171.7bp,59.148bp)  .. (190.63bp,59.148bp);
  \draw (172.8bp,48.648bp) node {\textit{a}};
  % Edge: q3 -> q2
  \draw [->] (281.95bp,75.138bp) .. controls (269.12bp,90.1bp) and (249.23bp,113.29bp)  .. (227.62bp,138.49bp);
  \draw (260.8bp,108.8bp) node {\textit{p}};
  % Edge: q0 -> q1
  \draw [->] (79.167bp,59.148bp) .. controls (86.981bp,59.148bp) and (96.075bp,59.148bp)  .. (114.64bp,59.148bp);
  \draw (96.8bp,48.648bp) node {\textit{p}};
  % Edge: q4 -> q3
  \draw [->] (239.24bp,59.148bp) .. controls (247.19bp,59.148bp) and (256.1bp,59.148bp)  .. (274.54bp,59.148bp);
  \draw (256.8bp,48.648bp) node {\textit{p}};
  % Edge: q3 -> q4
  \draw [->] (277.54bp,70.212bp) .. controls (272.22bp,73.046bp) and (266.21bp,75.7bp)  .. (260.3bp,77.148bp) .. controls (255.07bp,78.43bp) and (249.64bp,77.974bp)  .. (234.8bp,72.875bp);
  \draw (256.8bp,69.648bp) node {\textit{a}};
  % Edge: q4 -> q2
  \draw [->] (214.8bp,83.691bp) .. controls (214.8bp,95.837bp) and (214.8bp,110.78bp)  .. (214.8bp,134.0bp);
  \draw (218.3bp,108.8bp) node {\textit{a}};
  % Node: a
\begin{scope}
  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
  \pgfsetstrokecolor{strokecol}
  \definecolor{fillcol}{rgb}{0.0,0.0,0.0};
  \pgfsetfillcolor{fillcol}
  \filldraw [opacity=1] (1.8bp,59.15bp) ellipse (1.8bp and 1.8bp);
\end{scope}
  % Node: q0
\begin{scope}
  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
  \pgfsetstrokecolor{strokecol}
  \draw (58.8bp,59.15bp) ellipse (20.3bp and 20.3bp);
  \draw (58.8bp,59.148bp) node {$q_{0}$};
\end{scope}
  % Node: q3
\begin{scope}
  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
  \pgfsetstrokecolor{strokecol}
  \draw (294.8bp,59.15bp) ellipse (20.3bp and 20.3bp);
  \draw (294.8bp,59.148bp) node {$q_{3}$};
\end{scope}
  % Node: q2
\begin{scope}
  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
  \pgfsetstrokecolor{strokecol}
  \draw (214.8bp,154.44bp) ellipse (20.3bp and 20.3bp);
  \draw (214.8bp,154.44bp) node {$q_{2}$};
\end{scope}
  % Node: q4
\begin{scope}
  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
  \pgfsetstrokecolor{strokecol}
  \draw (214.8bp,59.15bp) ellipse (20.27bp and 20.27bp);
  \draw (214.8bp,59.15bp) ellipse (24.3bp and 24.3bp);
  \draw (214.8bp,59.148bp) node {$q_{4}$};
\end{scope}
  % Node: q1
\begin{scope}
  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
  \pgfsetstrokecolor{strokecol}
  \draw (134.8bp,59.15bp) ellipse (20.3bp and 20.3bp);
  \draw (134.8bp,59.148bp) node {$q_{1}$};
\end{scope}

\end{tikzpicture}

\end{center}
\end{example}

\newpage